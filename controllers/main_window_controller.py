from views.main_window_view import MenuItemWindow, MainWindowView
from views.settings_view import SettingsView
from models.bot_model import BotModel
from models.answer_strategy import MimicStrategy, NormalStrategy, EmojiStrategy
from models.list_element import MessageListElementModel
from datetime import datetime
from views.about_view import AboutView


class MainWindowController:
    def __init__(self):
        # init model
        self._model = None
        # init views
        self._main_view = None
        self._about_view = None
        self._settings_view = None
        self._history_view = None

    def _send_message(self):
        # =========== question ========
        question = self._main_view.elements['edit_text'].text()
        self._main_view.elements['edit_text'].setText('')
        question_timestamp = datetime.now()
        question_text = question_timestamp.strftime('%H:%M:%S') + ' | You: ' + question
        self._main_view.elements['conversation_list'].addItem(question_text)

        # =========== answer ===========
        answer = self._model.answer_strategy.generate_answer(question)
        answer_timestamp = datetime.now()
        answer_text = answer_timestamp.strftime('%H:%M:%S') + ' | ' + self._model.name + ': ' + answer
        self._main_view.elements['conversation_list'].addItem(answer_text)

        # =========== mlem =================
        list_element = MessageListElementModel(question, answer, question_timestamp, answer_timestamp)
        self._model.add_to_list(list_element)
        self._main_view.elements['conversation_list'].scrollToBottom()

    def _on_menu_click(self, menu_item):
        signal = menu_item.text()

        if signal == 'History':
            self._history_view.window.show()

        elif signal == 'Settings':
            self._settings_view.window.show()

        elif signal == "About":
            self._about_view.window.show()

    def set_model(self, model):
        self._model = model

    def set_main_view(self, view):
        self._main_view = view

    def set_about_view(self, view):
        self._about_view = view

    def set_settings_view(self, view):
        self._settings_view = view

    def set_history_view(self, view):
        self._history_view = view

    def init_listeners(self):
        # init listeners
        self._main_view.elements['send_button'].clicked.connect(self._send_message)
        self._main_view.elements['edit_text'].returnPressed.connect(self._send_message)
        self._main_view.elements['menu_action'].triggered.connect(self._on_menu_click)
