from models.sorting_algorithm import SortByDateDescending, SortByDateAscending
from models.builder import BuilderHTML, BuilderCSV, Builder
from models.filter import FilterChain, KeywordFilter, BetweenDatesFilter, WrongRequestFilter
import os
from datetime import datetime
from PyQt5.QtCore import QDateTime, Qt


class HistoryController:
    def __init__(self):
        self._model = None
        self._history_view = None
        self._filter_view = None 
        self._builder = None

    def set_model(self, model):
        self._model = model

    def set_history_view(self, view):
        self._history_view = view

    def set_filter_view(self, view):
        self._filter_view = view

    def _build(self):
        conversations = self._model.conversations
        
        self._builder.add_header()
        self._builder.add_column('question timestamp')
        self._builder.add_column('question')
        self._builder.add_column('answer timestamp')
        self._builder.add_column('answer')
        self._builder.add_row()
        
        for conv in conversations:
            for element in conv: 
                self._builder.add_column(element.question_timestamp)
                self._builder.add_column(element.question.replace(',', ' '))
                self._builder.add_column(element.answer_timestamp)
                self._builder.add_column(element.answer.replace(',', ' '))
                self._builder.add_row()

    def export_csv(self):
        self._builder = BuilderCSV()
        self._build()     
        path = os.path.abspath('./conversations.csv')
        with open(path, 'w+') as file:
            file.write(self._builder.get_csv())

    def export_html(self):
        self._builder = BuilderHTML()
        self._build()
        path = os.path.abspath('./conversations.html')
        with open(path, 'w+') as file:
            file.write(self._builder.get_html())

    def _on_item_click(self):
        conversations_list = self._history_view.elements['conversations_list']
        detailed_list_view = self._history_view.elements['detailed_list']
        detailed_list_view.clear()
        index = conversations_list.currentRow()
        detailed_list = self._model.get_conversation(index)
        for item in detailed_list:
            detailed_list_view.addItem(item.to_string())

    def _on_menu_item_click(self, menu_item):
        conversations_list = self._history_view.elements['conversations_list']
        detailed_list_view = self._history_view.elements['detailed_list']
        index = conversations_list.currentRow()
        signal = menu_item.text()
        if signal == 'Sort by date descending':
            detailed_list_view.clear()
            algorithm = SortByDateDescending()
            self._sort_by_date(algorithm, detailed_list_view, index)
        elif signal == 'Sort by date ascending':
            detailed_list_view.clear()
            algorithm = SortByDateAscending()
            self._sort_by_date(algorithm, detailed_list_view, index)
        elif signal == 'Add filter':
            self._filter_view.window.show()
        elif signal == 'Export to .html':
            self.export_html()
        elif signal == 'Export to .csv':
            self.export_csv()

    def _sort_by_date(self, algorithm, detailed_list_view, index):
        sorted_list = algorithm.sort_conversation(self._model.get_conversation(index))
        for item in sorted_list:
            detailed_list_view.addItem(item.to_string())

    def _add_keyword_filter(self):
        keyword = self._filter_view.elements['keyword_edit_text'].text()
        if keyword:
            self._filter_view.elements['filter_list'].addItem('Keyword-filter: ' + keyword)
            if not self._model.filter_chain:
                self._model.filter_chain = FilterChain()
            keyword_filter = KeywordFilter(keyword)
            self._model.filter_chain.add_filter(keyword_filter)

    def _add_between_dates_filter(self):
        start_date = self._filter_view.elements['start_date_edit'].dateTime()
        end_date = self._filter_view.elements['end_date_edit'].dateTime()

        self._filter_view.elements['filter_list'].addItem('Datetime-filter: ' + 'from: '
                                                          + start_date.toString(Qt.ISODate).replace('T', ' ') + ' to: ' + start_date.toString(Qt.ISODate).replace('T', ' '))

        if not self._model.filter_chain:
            self._model.filter_chain = FilterChain()
        dates_filter = BetweenDatesFilter(start_date, end_date)
        self._model.filter_chain.add_filter(dates_filter)

    def _add_wrong_request_filter(self):
        self._filter_view.elements['filter_list'].addItem('Wrong-request-filter:')
        if not self._model.filter_chain:
            self._model.filter_chain = FilterChain()
        wrong_request_filter = WrongRequestFilter()
        self._model.filter_chain.add_filter(wrong_request_filter)

    def _execute_filters(self):
        if not self._model.filter_chain:
            self._filter_view.window.close()
            return
        conversations_list = self._history_view.elements['conversations_list']
        detailed_list_view = self._history_view.elements['detailed_list']
        index = conversations_list.currentRow()

        selected_list = self._model.get_conversation(index)
        filtered_list = self._model.filter_chain.execute(selected_list)
        detailed_list_view.clear()

        for item in filtered_list:
            detailed_list_view.addItem(item.to_string())

        self._model.filter_chain = None
        self._filter_view.elements['filter_list'].clear()

        self._filter_view.window.close()

    def _delete_filter(self):
        index = self._filter_view.elements['filter_list'].currentRow()
        if self._model.filter_chain:
            self._model.filter_chain.remove_filter(index)
        index = self._filter_view.elements['filter_list'].takeItem(index)


    def init_listeners(self):
        conversations_list = self._history_view.elements['conversations_list']
        #conversations_list.addItems(self._model.conversations)

        for conv in self._model.conversations:
            if(len(conv)> 0):
                conversations_list.addItem(str(conv[0].question_timestamp))

        conversations_list.itemClicked.connect(self._on_item_click)

        self._history_view.elements['menu_bar'].triggered.connect(self._on_menu_item_click)
        self._filter_view.elements['keyword_button'].clicked.connect(self._add_keyword_filter)
        self._filter_view.elements['confirm_button'].clicked.connect(self._execute_filters)
        self._filter_view.elements['wrong_request_button'].clicked.connect(self._add_wrong_request_filter)
        self._filter_view.elements['dates_button'].clicked.connect(self._add_between_dates_filter)
        self._filter_view.elements['delete_button'].clicked.connect(self._delete_filter)
        self._filter_view.elements['filter_list'].doubleClicked.connect(self._delete_filter)