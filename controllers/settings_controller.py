from models.answer_strategy import NormalStrategy, EmojiStrategy, ArnoldStrategy, MimicStrategy


class SettingsController:
    def __init__(self):
        self._model = None
        self._view = None

    def set_model(self, model):
        self._model = model

    def set_view(self, view):
        self._view = view

    def _submit(self):
        radio_buttons = self._view.elements['radio_buttons']

        if radio_buttons[0].isChecked():
            self._model.answer_strategy = NormalStrategy()
        elif radio_buttons[1].isChecked():
            self._model.answer_strategy = EmojiStrategy()
        elif radio_buttons[2].isChecked():
            self._model.answer_strategy = ArnoldStrategy()
        else:
            self._model.answer_strategy = MimicStrategy()

    def _close_window(self):
        self._view.window.close()

    def init_listeners(self):
        self._view.elements['submit_button'].clicked.connect(self._submit)
        self._view.elements['submit_button'].clicked.connect(self._close_window)

