from views.about_view import AboutView


class AboutController:
    def __init__(self):
        self._view = None

    def set_view(self, view):
        self._view = view

