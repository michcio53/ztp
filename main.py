from controllers.main_window_controller import MainWindowController
from controllers.about_controller import AboutController
from controllers.settings_controller import SettingsController
from controllers.history_controller import HistoryController
from views.main_window_view import MainWindowView
from views.about_view import AboutView
from views.settings_view import SettingsView
from views.history_view import HistoryView
from views.filter_view import FilterView
from models.bot_model import BotModel
from models.answer_strategy import NormalStrategy
from models.history_model import HistoryModel
import sys
from PyQt5.QtWidgets import QApplication

# start app
app = QApplication(sys.argv)

# create models
bot_model = BotModel("SuperBot", NormalStrategy())
history_model = HistoryModel()
#history_model.mockup()

#load conversations
history_model.load_conversations()

# create views
main_view = MainWindowView()
about_view = AboutView()
settings_view = SettingsView()
history_view = HistoryView()
filter_view = FilterView()

# create controllers
main_controller = MainWindowController()
about_controller = AboutController()
settings_controller = SettingsController()
history_controller = HistoryController()

# init main controller
main_controller.set_model(bot_model)
main_controller.set_main_view(main_view)
main_controller.set_about_view(about_view)
main_controller.set_settings_view(settings_view)
main_controller.set_history_view(history_view)
main_controller.init_listeners()

# init about controller
about_controller.set_view(about_view)

# init settings controller
settings_controller.set_model(bot_model)
settings_controller.set_view(settings_view)
settings_controller.init_listeners()

# init history controller
history_controller.set_model(history_model)
history_controller.set_history_view(history_view)
history_controller.set_filter_view(filter_view)
history_controller.init_listeners()

#app close
def save():
    if len(bot_model.list_messages) != 0:
        history_model.add_to_conversations(bot_model.list_messages)
        history_model.save_conversations()
    

app.aboutToQuit.connect(save)


# exit app
sys.exit(app.exec_())
