import sqlite3, os
from abc import ABCMeta, abstractmethod
from chatterbot import ChatBot


class DatabaseModel(metaclass=ABCMeta):
    @abstractmethod
    def execute_query(self, query):
        pass

    @abstractmethod
    def get_answer(self, question):
        pass


class DatabaseProxy(DatabaseModel):
    def __init__(self, real_database):
        self._real_database = real_database

    def execute_query(self, query):
        if 'drop' in query.lower():
            return 'Cannot use DROP'
        elif query == '':
            return 'Cannot be empty'
        else:
            return self._real_database.execute_query(query)
    
    def get_answer(self, question):
        if 'drop' in question.lower():
            return 'Don\'t be so smart'
        elif question == '':
            return 'You have nothing to say?'
        elif len(question) >= 140:
            return 'Too long; didn\'t read lol'
        else:
            return self._real_database.get_answer(question)


class RealDatabase(DatabaseModel):
    class _RealDatabase(DatabaseModel):
        def __init__(self):
            path = os.path.abspath('./data/database.sqlite3')
            self._bot = ChatBot(
                'Bot',
                storage_adapter='chatterbot.storage.SQLStorageAdapter',
                trainer='chatterbot.trainers.ChatterBotCorpusTrainer',
                logic_adapters=[
                    {
                        'import_path': 'chatterbot.logic.BestMatch'
                    },
                    {
                        'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                        'threshold': 0.5,
                        'default_response': 'I am sorry, but I do not understand.'
                    },
                    {
                        'import_path': 'chatterbot.logic.MathematicalEvaluation',
                    }
                ],
                database=path
            )
            #self._bot.train('chatterbot.corpus.english')
        

        def execute_query(self, query):
            connection = sqlite3.connect('./data/database.sqlite3')
            cursor = connection.cursor()
            result = ''
            try:
                cursor.execute(query)
                for row in cursor:
                    result += (str(row) + '\n')
                connection.commit()
                connection.close()
                return result
            except sqlite3.Error as e:
                print('Error occured: ' + e.args[0])

            return 'Couldn\'t fetch data'
            
        def get_answer(self, question):
            return self._bot.get_response(question).text

    _instance = None

    def __init__(self):
        if self._instance is None:
            RealDatabase._instance = RealDatabase._RealDatabase()

    def __getattr__(self, item):
        return getattr(self._instance, item)

    def execute_query(self, query):
        return self._instance.execute_query(query)

    def get_answer(self, question):
        return self._instance.get_answer(question)
