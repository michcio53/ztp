class BotModel(object):
    def __init__(self, name, answer_strategy):
        self._name = name
        self._answer_strategy = answer_strategy
        self._list_messages = []

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def answer_strategy(self):
        return self._answer_strategy

    @answer_strategy.setter
    def answer_strategy(self, answer_strategy):
        self._answer_strategy = answer_strategy

    @property
    def list_messages(self):
        return self._list_messages

    @list_messages.setter
    def list_messages(self, list_messages):
        self._list_messages = list_messages

    def add_to_list(self, message):
        self._list_messages.append(message)
