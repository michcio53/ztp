class MessageListElementModel:

    def __init__(self, question=None, answer=None, question_timestamp=None, answer_timestamp=None):
        self._question = question
        self._answer = answer
        self._question_timestamp = question_timestamp
        self._answer_timestamp = answer_timestamp

    def __str__(self):
        return ('{\n' + '\t' +
                'question: ' + self._question + '\n' + '\t' +
                'answer: ' + self._answer + '\n' + '\t' +
                'question timestamp: ' + self._question_timestamp + '\n' + '\t' +
                'answer timestamp: ' + self._answer_timestamp +
                '\n}')

    def to_string(self):
        return (self._question_timestamp.strftime('%H:%M:%S') + ' | You: ' + self._question + '\n' +
                self._answer_timestamp.strftime('%H:%M:%S') + ' | Bot: ' + self.answer)

    def to_dict(self):
        message_dict = {}
        message_dict['_question'] = self._question
        message_dict['_answer'] = self._answer
        message_dict['_question_timestamp'] = self._question_timestamp.strftime('%H:%M:%S %d-%m-%Y')
        message_dict['_answer_timestamp'] = self._answer_timestamp.strftime('%H:%M:%S %d-%m-%Y')
        return message_dict

    @property
    def question(self):
        return self._question

    @question.setter
    def question(self, question):
        self._question = question

    @property
    def answer(self):
        return self._answer

    @answer.setter
    def answer(self, answer):
        self._answer = answer

    @property
    def question_timestamp(self):
        return self._question_timestamp

    @question_timestamp.setter
    def question_timestamp(self, question_timestamp):
        self._question_timestamp = question_timestamp

    @property
    def answer_timestamp(self):
        return self._answer_timestamp

    @answer_timestamp.setter
    def answer_timestamp(self, answer_timestamp):
        self._answer_timestamp = answer_timestamp









