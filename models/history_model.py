from models.filter import FilterChain
from models.list_element import MessageListElementModel
import datetime, os, json
from datetime import datetime


class HistoryModel:
    def __init__(self):
        self._conversations = []
        self._filter_chain = None

    @property
    def conversations(self):
        return self._conversations

    @conversations.setter
    def conversations(self, conversations_list):
        self._conversations = conversations_list

    @property
    def filter_chain(self):
        return self._filter_chain

    @filter_chain.setter
    def filter_chain(self, filter_chain):
        self._filter_chain = filter_chain

    def get_conversation(self, index):
        return self._conversations[index]

    def add_to_conversations(self, conversation):
        self._conversations.append(conversation)

    def load_conversations(self):
        path = os.path.abspath('./data/conversations.json')
        with open(path, 'r') as file:
            if(os.stat(path).st_size != 0):
                data = json.load(file)
                self.read_data(data)

    def read_data(self, data):
        for item in data:
            keys = item.keys()
            conversation = []
            for key in keys:
                dict = item[key]
                for obj in dict:
                    question = obj['_question']
                    answer = obj['_answer']
                    question_time = datetime.strptime(obj['_question_timestamp'], '%H:%M:%S %d-%m-%Y') 
                    answer_time = datetime.strptime(obj['_answer_timestamp'], '%H:%M:%S %d-%m-%Y')
                    message = MessageListElementModel(question, answer, question_time, answer_time)
                    conversation.append(message)
            self._conversations.append(conversation)

    def save_conversations(self):
        formatted_data = []
        for idx, item in enumerate(self._conversations):
            obj_name = 'conversation' + str(idx)
            conv_dict = {}
            conv_dict[obj_name] = []
            formatted_data.append(conv_dict)
            for message in item:
                conv_dict[obj_name].append(message.to_dict())
            
        path = os.path.abspath('./data/conversations.json')
        with open(path, 'w') as file:
            json.dump(formatted_data, file)

    def mockup(self):
        date1 = datetime(2010, 1, 1, 1, 1, 1)
        date2 = datetime(2020, 1, 1, 1, 1, 1)
        date3 = datetime(2030, 1, 1, 1, 1, 1)
        date4 = datetime(2040, 1, 1, 1, 1, 1)

        mlem1 = MessageListElementModel('question', 'answer', date1, date1)
        mlem2 = MessageListElementModel('e', 'f', date2, date2)
        mlem3 = MessageListElementModel('a', 'f', date3, date3)
        mlem4 = MessageListElementModel('a', 'error', date4, date4)

        conv1 = [mlem1, mlem2, mlem3, mlem4]

        date5 = datetime(2050, 1, 1, 1, 1, 1)
        date6 = datetime(2060, 1, 1, 1, 1, 1)
        date7 = datetime(2070, 1, 1, 1, 1, 1)
        date8 = datetime(2080, 1, 1, 1, 1, 1)

        mlem5 = MessageListElementModel('second_question', 'second_answer', date5, date5)
        mlem6 = MessageListElementModel('e', 'f', date6, date6)
        mlem7 = MessageListElementModel('a', 'f', date7, date7)
        mlem8 = MessageListElementModel('a', 'error', date8, date8)

        conv2 = [mlem5, mlem6, mlem7, mlem8]

        self._conversations.append(conv1)
        self._conversations.append(conv2)