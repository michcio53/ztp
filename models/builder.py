import abc


class Builder(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def add_header(self):
        pass

    @abc.abstractmethod
    def add_column(self, text):
        pass

    @abc.abstractmethod
    def add_row(self):
        pass


class BuilderHTML(Builder):
    def __init__(self):
        self._header_exist = False
        self._first_exist = True
        self._string = '<table border="1">\n'

    def add_header(self):
        if self._first_exist is False:
            return
        self._first_exist = False
        self._header_exist = True
        self._string += '<tr>\n'

    def add_column(self, text):
        if self._header_exist is True:
            self._string += ('<th>' + str(text) + '</th>\n')
        else:
            self._string += ('<td>' + str(text) + '</td>\n')

    def add_row(self):
        if self._first_exist is False:
            self._string += '</tr>\n'
        self._first_exist = False
        self._header_exist = False
        self._string += '<tr>\n'

    def get_html(self):
        if self._first_exist is False:
            self._string += '</tr>\n'
        self._string += '</table>\n'
        return self._string


class BuilderCSV(Builder):
    def __init__(self):
        self._string = ''
        self._header_exist = False
        self._first_exist = False

    def add_header(self):
        self._header_exist = True

    def add_column(self, text):
        if self._first_exist is True:
            self._string += ','
        self._string += str(text)
        self._first_exist = True

    def add_row(self):
        self._string += '\n'
        self._first_exist = False

    def get_csv(self):
        return self._string


'''
_file = open('conversation.html', 'w')
builder = BuilderHTML()
builder.add_header()
builder.add_column('123asd')
builder.add_column('asd123')
builder.add_row()
builder.add_column('asd123')
builder.add_column('123asd')
_file.write(builder.get_html())


_file = open('conversation.csv', 'w')
builder = BuilderCSV()
builder.add_header()
builder.add_column('123asd')
builder.add_column('asd123')
builder.add_row()
builder.add_column('asd123')
builder.add_column('123asd')
_file.write(builder.get_csv())
'''
