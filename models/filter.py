import abc


class FilterChain:
    def __init__(self):
        self._filters = []

    def add_filter(self, specific_filter):
        self._filters.append(specific_filter)

    def remove_filter(self, index):
        if len(self._filters) > 0:
            self._filters.pop(index)

    def execute(self, conversation):
        filtered = list(conversation)
        for f in self._filters:
            filtered = f.meet_criteria(filtered)
        return filtered


class Filter(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def meet_criteria(self, conversation):
        pass


class KeywordFilter(Filter):
    def __init__(self, keyword):
        self._keyword = keyword

    def meet_criteria(self, conversation):
        filtered = []
        for element in conversation:
            if (self._keyword in element.question.split()
                    or self._keyword in element.answer.split()):
                filtered.append(element)
        return filtered


class BetweenDatesFilter(Filter):
    def __init__(self, date_start, date_end):
        self._date_start = date_start
        self._date_end = date_end

    def meet_criteria(self, conversation):
        filtered = []
        for element in conversation:
            if (self._date_start <= element.question_timestamp <= self._date_end
                    or self._date_start <= element.question_timestamp <= self._date_end):
                filtered.append(element)
        return filtered


class WrongRequestFilter(Filter):
    def meet_criteria(self, conversation):
        filtered = []
        sentences = [
            "Don't be so smart",
            "You have nothing to say?",
            "Too long; didn't read lol"
        ]

        for element in conversation:
            if any(sentence in element.answer for sentence in sentences):
                filtered.append(element)

        return filtered


'''
date1 = datetime.datetime(2000, 1, 1, 1, 1, 1)
date2 = datetime.datetime(2010, 1, 1, 1, 1, 1)
date3 = datetime.datetime(2020, 1, 1, 1, 1, 1)
date4 = datetime.datetime(2030, 1, 1, 1, 1, 1)

mlem1 = MessageListElementModel('a', 'b', date1, date1)
mlem2 = MessageListElementModel('e', 'f', date2, date2)
mlem3 = MessageListElementModel('a', 'f', date3, date3)
mlem4 = MessageListElementModel('a', 'error', date4, date4)

conv = [mlem1, mlem2, mlem3, mlem4]

chain = FilterChain()

chain.add_filter(KeywordFilter('a'))
#chain.add_filter(KeywordFilter('b'))
#chain.add_filter(WrongRequest())
chain.add_filter(BetweenDatesFilter(date4, date4))

result = chain.execute(conv)

for x in result:
    print(x.__dict__)

'''


