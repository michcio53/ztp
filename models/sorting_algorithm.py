import abc
from functools import cmp_to_key
from models.list_element import MessageListElementModel
import datetime


class SortingAlgorithm(metaclass=abc.ABCMeta):
    def sort_conversation(self, conversation):
        return sorted(conversation, key=cmp_to_key(self._compare))

    @abc.abstractmethod
    def _compare(self, first, second):
        pass


class SortByDateDescending(SortingAlgorithm):
    def _compare(self, first, second):
        return (second.question_timestamp - first.question_timestamp).total_seconds()


class SortByDateAscending(SortingAlgorithm):
    def _compare(self, first, second):
        return (first.question_timestamp - second.question_timestamp).total_seconds()


'''
alg = SortByDateAscending()

date1 = datetime.datetime(2010, 1, 1, 1, 1, 1)
date2 = datetime.datetime(2020, 1, 1, 1, 1, 1)
date3 = datetime.datetime(2030, 1, 1, 1, 1, 1)
date4 = datetime.datetime(2040, 1, 1, 1, 1, 1)

mlem1 = MessageListElementModel('a', 'b', date2, date2)
mlem2 = MessageListElementModel('e', 'f', date4, date4)
mlem3 = MessageListElementModel('a', 'f', date3, date3)
mlem4 = MessageListElementModel('a', 'error', date1, date1)

conv = [mlem1, mlem2, mlem3, mlem4]


sorted_list = alg.sort_conversation(conv)

for x in sorted_list:
    print(x.__dict__)

'''





