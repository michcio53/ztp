import abc, json, os, random
from models.database_model import RealDatabase, DatabaseProxy



class AnswerStrategy(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def generate_answer(self, question):
        pass


class NormalStrategy(AnswerStrategy):
    def __init__(self):
        self._db = DatabaseProxy(RealDatabase())

    def generate_answer(self, question):
        return self._db.get_answer(question)


class EmojiStrategy(AnswerStrategy):
    def __init__(self):
        self._db = DatabaseProxy(RealDatabase())
        path = os.path.abspath('./data/emojis.json')
        with open(path, encoding='utf8') as file:
            self._emoji_data = json.load(file)
        

    def generate_answer(self, question):
        response = self._db.get_answer(question)
        response_array = response.split(' ')
        for idx, word in enumerate(response_array):
            for item in self._emoji_data.values():
                for keyword in item['keywords']:
                    if keyword == word.lower().replace('.', '').replace(',', ''):
                        response_array[idx] = item['char']

        response = ' '.join(response_array)
        return response


class ArnoldStrategy(AnswerStrategy):
    def __init__(self):
        path = os.path.abspath('./data/answers.json')
        with open(path) as file:
            data = json.load(file)
        self._answers = data['answers']
        
    def generate_answer(self, question):
        return random.choice(self._answers)


class MimicStrategy(AnswerStrategy):
    def generate_answer(self, question):
        return question
