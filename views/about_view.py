from PyQt5.QtWidgets import *


class AboutView:
    def __init__(self):
        self._elements = {}
        self._window = AboutWindow()
        self._elements['botname_label'] = self._window.bot_name
        self._elements['title_label'] = self._window.title
        self._elements['authors_label'] = self._window.authors
        self._elements['city_label'] = self._window.city

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, elements):
        self._elements = elements

    @property
    def window(self):
        return self._window

    @window.setter
    def window(self, window):
        self._window = window


class AboutWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.bot_name = QLabel("Ava the super bot machine")
        self.title = QLabel("Bot Designed By:")
        self.authors = QLabel("Piotr Szymański\nMichał Sokół\nJakub Sawicki\nMarcin Węglicki")
        self.city = QLabel("In Bialystok 2018")
        self.init_ui()

    def init_ui(self):
        v_box = QVBoxLayout()
        h_box = QHBoxLayout()
        h_box.addStretch()
        h_box.addWidget(self.bot_name)
        h_box.addStretch()
        v_box.addLayout(h_box)
        v_box.addWidget(self.title)
        v_box.addWidget(self.authors)
        v_box.addWidget(self.city)
        self.setLayout(v_box)
        self.setWindowTitle("About")
        self.left_offset = 50
        self.right_offset = 50
        self.width = 0
        self.height = 0
        self.setGeometry(self.left_offset, self.right_offset, self.width, self.height)

