from PyQt5.QtWidgets import *
import sys


class HistoryView:
    def __init__(self):
        self._elements = {}
        self._window = HistoryWindow()
        self._elements['menu_bar'] = self._window.menu_bar
        self._elements['conversations_list'] = self._window.basic_layout.main_history
        self._elements['detailed_list'] = self._window.basic_layout.detailed_history

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, elements):
        self._elements = elements

    @property
    def window(self):
        return self._window

    @window.setter
    def window(self, window):
        self._window = window


class BasicLayout(QWidget):
    def __init__(self):
        super().__init__()
        self.main_history = QListWidget(self)
        self.detailed_history = QListWidget(self)
        self.init_ui()

    def init_ui(self):
        horizontal_layout = QHBoxLayout()
        horizontal_layout.addWidget(self.main_history)
        horizontal_layout.addWidget(self.detailed_history)
        self.setLayout(horizontal_layout)


class HistoryWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.left_offset = 50
        self.right_offset = 50
        self.width = 640
        self.height = 480
        self.menu_bar = self.menuBar()
        self.basic_layout = BasicLayout()
        self.setCentralWidget(self.basic_layout)
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("History")
        sorting = self.menu_bar.addMenu("Sort")
        filter_option = self.menu_bar.addMenu("Filter")
        exporting = self.menu_bar.addMenu("Export")

        sort_down_action = QAction("Sort by date descending", self)
        sort_up_action = QAction("Sort by date ascending", self)

        sorting.addAction(sort_down_action)
        sorting.addAction(sort_up_action)

        filter_action = QAction("Add filter", self)

        filter_option.addAction(filter_action)

        exporting.addAction(QAction("Export to .html", self))
        exporting.addAction(QAction("Export to .csv", self))
        self.setGeometry(self.left_offset, self.right_offset, self.width, self.height)










