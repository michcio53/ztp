import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, qApp, QListWidget, QVBoxLayout, QLineEdit, QWidget, \
    QPushButton, QHBoxLayout
from views.settings_view import SettingsView
from views.history_view import HistoryView
from views.about_view import AboutView


class MainWindowView:
    def __init__(self):
        self._elements = {}
        self._window = MenuItemWindow()
        self._elements['send_button'] = self._window.basic_layout.send_button
        self._elements['edit_text'] = self._window.basic_layout.edit_text_field
        self._elements['conversation_list'] = self._window.basic_layout.conversation_list
        self._elements['menu_action'] = self._window.menu_action

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, elements):
        self._elements = elements


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.send_button = QPushButton("Send")
        self.conversation_list = QListWidget(self)
        self.edit_text_field = QLineEdit(self)
        self.edit_text_field.setFixedHeight(25)
        self.init_ui()

    def init_ui(self):
        vertical_layout = QVBoxLayout()
        horizontal_layout = QHBoxLayout()
        horizontal_layout.addWidget(self.edit_text_field)
        horizontal_layout.addWidget(self.send_button)
        vertical_layout.addWidget(self.conversation_list)
        vertical_layout.addLayout(horizontal_layout)
        self.setLayout(vertical_layout)


class MenuItemWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.left_offset = 50
        self.right_offset = 50
        self.width = 640
        self.height = 480
        self.basic_layout = MainWindow()
        self.menu_bar = self.menuBar()
        self.menu_action = self.menu_bar.addMenu("Menu")
        self.setCentralWidget(self.basic_layout)
        self.init_ui()

    def closeEvent(self, event):
        qApp.closeAllWindows()

    def init_ui(self):
        history_action = QAction("History", self)
        settings_action = QAction("Settings", self)
        about_action = QAction("About", self)
        self.menu_action.addAction(history_action)
        self.menu_action.addAction(settings_action)
        self.menu_action.addAction(about_action)
        self.setWindowTitle("ChatBot")
        self.setGeometry(self.left_offset, self.right_offset, self.width, self.height)
        self.show()
