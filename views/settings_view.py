from PyQt5.QtWidgets import *
import sys


class SettingsView:
    def __init__(self):
        self._elements = {}
        self.window = SettingsWindow()
        self._elements['radio_buttons'] = self.window.radio_buttons
        self._elements['submit_button'] = self.window.submit_button

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, elements):
        self._elements = elements


class SettingsWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.label = QLabel("Choose bot answer strategy")
        self.radio_button_normal = QRadioButton("Normal")
        self.radio_button_emoji = QRadioButton("Emoji :)")
        self.radio_button_arnold = QRadioButton("Arnold")
        self.radio_button_mimic = QRadioButton("Mimic")
        self.radio_buttons = [
            self.radio_button_normal,
            self.radio_button_emoji,
            self.radio_button_arnold,
            self.radio_button_mimic
        ]
        self.submit_button = QPushButton("Submit")
        self.init_ui()

    def init_ui(self):
        v_box = QVBoxLayout()
        v_box.addWidget(self.label)
        self.radio_button_normal.setChecked(True)
        self.radio_button_normal.strategy = "Normal"
        v_box.addWidget(self.radio_button_normal)
        self.radio_button_emoji.strategy = "Emoji"
        v_box.addWidget(self.radio_button_emoji)
        self.radio_button_arnold.strategy = "Arnold"
        v_box.addWidget(self.radio_button_arnold)
        self.radio_button_mimic.strategy = "Mimic"
        v_box.addWidget(self.radio_button_mimic)
        v_box.addWidget(self.submit_button)
        self.setLayout(v_box)
        self.setWindowTitle("Settings")
        self.left_offset = 50
        self.right_offset = 50
        self.width = 0
        self.height = 0
        self.setGeometry(self.left_offset, self.right_offset, self.width, self.height)





