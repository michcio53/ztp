from PyQt5.QtWidgets import *
from PyQt5.QtCore import QDateTime
import sys


class FilterView:
    def __init__(self):
        self._elements = {}
        self.window = FilterWindow()
        self._elements['keyword_button'] = self.window.keyword_btn
        self._elements['dates_button'] = self.window.dates_btn
        self._elements['wrong_request_button'] = self.window.wrong_request_btn
        self._elements['filter_list'] = self.window.filter_list
        self._elements['confirm_button'] = self.window.confirm_btn
        self._elements['keyword_edit_text'] = self.window.keyword_edit_text
        self._elements['start_date_edit'] = self.window.start_date_edit
        self._elements['end_date_edit'] = self.window.end_date_edit
        self._elements['delete_button'] = self.window.delete_btn


    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, elements):
        self._elements = elements


class FilterWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.keyword_btn = QPushButton('Add keyword filter')
        self.dates_btn = QPushButton('Add between dates filter')
        self.wrong_request_btn = QPushButton('Add wrong request filter')
        self.filter_list = QListWidget(self)
        self.confirm_btn = QPushButton('Confirm')
        self.keyword_edit_text = QLineEdit(self)
        self.start_date_edit = QDateTimeEdit(self)
        self.end_date_edit = QDateTimeEdit(self)
        self.keyword_edit_text.setFixedHeight(25)
        self.keyword_btn.setFixedWidth(200)
        self.dates_btn.setFixedWidth(200)
        self.wrong_request_btn.setFixedWidth(200)
        self.delete_btn = QPushButton('Delete selected filter')
        self.delete_btn.setFixedWidth(200)

        self.start_date_edit.setDisplayFormat('dd.MM.yyyy HH:mm:ss')
        self.end_date_edit.setDisplayFormat('dd.MM.yyyy HH:mm:ss')
        self.start_date_edit.setDateTime(QDateTime.currentDateTime())
        self.end_date_edit.setDateTime(QDateTime.currentDateTime())


        self.init_ui()

    def init_ui(self):
        v_box = QVBoxLayout()
        v_box.addWidget(self.filter_list)

        h_box = QHBoxLayout()
        h_box.addWidget(self.keyword_btn)
        h_box.addWidget(self.keyword_edit_text)
        v_box.addLayout(h_box)

        h_box = QHBoxLayout()
        h_box.addWidget(self.dates_btn)
        h_box.addWidget(self.start_date_edit)
        h_box.addWidget(self.end_date_edit)

        v_box.addLayout(h_box)

        v_box.addWidget(self.wrong_request_btn)
        v_box.addWidget(self.delete_btn)

        v_box.addStretch(1)
        v_box.addWidget(self.confirm_btn)

        self.setLayout(v_box)
        self.setWindowTitle("Filter")




